const express = require('express');
const router = express.Router();
const controller = require('../controller/controller');
require('../db/connection');


router.post("/api/create", controller.create)
router.post("/api/getUser", controller.getUser)
router.post("/api/testAttempted", controller.testAttempted)
router.post("/api/kickout", controller.kickedOut)
router.post('/api/isuservalid', controller.isUserValid)
router.post('/api/pushTableData', controller.pushTableData)
router.post('/api/trainingData', controller.trainingData)
router.post('/api/officialData', controller.officialData)
router.post('/api/questionData', controller.questionData)
router.post("/api/saveData", controller.saveData)
router.post("/api/saveDelegationDecision", controller.saveDelegationDecision)


// router.get("/api/account", authenticate, controller.account)
// router.get("/api/getResult", controller.getResult)
// router.post("/api/login", controller.login)
// router.post("/api/testAttempted", controller.testAttempted)
// router.post("/api/learningAttempted", controller.learningAttempted)
// router.post("/api/updateLearningRound", controller.updateLearningRound)
//register using async await
// router.post("/api/signup", controller.signup)
// router.get("/api/logout", controller.logout)

module.exports = router
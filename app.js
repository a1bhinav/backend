const dotenv = require("dotenv");
const express = require("express");
const app = express();
const port = process.env.PORT || 10000;
const mongoose = require('mongoose');
const cors = require('cors')
//dotenv is a zero-dependency module that loads 
//environment variables from a . env file into process.env
dotenv.config({path: './config.env'});

require('./db/connection');
const User = require('./models/userSchema');

app.use(cors({origin: "https://human-computer-interaction-mwae.onrender.com"}));

//linking our router files to make routing easy
app.use(express.json());
app.use(require('./routes/admin'));


// middlewares
app.use(express.json({ extended: false }));


if (process.env.NODE_ENV == "production") {
    app.use(express.static("keeper-app/build")); //this is the react app build folder
    // const path = require("path");
    // app.get("*", (req, res) => {
    //     res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
    // });
}


app.listen(port, ()=>{
    console.log(`port started at ${port}`);
})
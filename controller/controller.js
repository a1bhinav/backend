"use strict";

const dotenv = require("dotenv");

const User = require("../models/userSchema");

exports.getUser = async (req, res) => {
  const { pid } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID: pid });
    if (rootUser) {
      res.status(201).json(rootUser);
    } else {
      res.status(400).json({ message: "User Not Found" });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.create = async (req, res) => {
  const { prolificPid, studyId, sessionId, random } = req.body;

  if (prolificPid != null) {
    try {
      const user = new User({ P_ID: prolificPid });
      await user.save();
      console.log(user);
      const P_ID = user.P_ID
      const rootUser = await User.findOne({ P_ID });
      if (rootUser) {
        const userData = {
          prolificPid: prolificPid,
          studyId: studyId,
          sessionId: sessionId,
        }
        await User.updateOne(
          { P_ID },
          {
            $set: {
              userData: userData,
              treatment: random
            },
          }
        );
      }
      res.status(201).json({ P_ID: P_ID, message: "User created successfully" });
    } catch (err) {
      console.log(err);
    }
  } else {
    res.status(400).json({ message: "Invalid Prolific ID" })
  }

};

exports.testAttempted = async (req, res) => {
  const { P_ID } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID });

    if (rootUser) {
      await User.updateOne(
        { P_ID },
        {
          $set: {
            testAttempted: true,
          },
        }
      );

      return res
        .status(200)
        .json({ message: "testAttempted switched to true" });
    }
  } catch (err) {
    return res.status(401).json({ error: "Couldn't update testAttempted" });
  }
};

exports.kickedOut = async (req, res) => {
  const { P_ID } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID });

    if (rootUser) {
      await User.updateOne(
        { P_ID },
        {
          $set: {
            kickedOut: true,
          },
        }
      );

      return res
        .status(200)
        .json({ message: "User has been kicked out" });
    }
  } catch (err) {
    return res.status(401).json({ error: "Couldn't update Kickout" });
  }
};

exports.isUserValid = async (req, res) => {
  const { P_ID } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID });
    if (rootUser.kickedOut === true) {
      return res.status(400).json({ message: "Invalid User" });
    } else if (rootUser.kickedOut === false) {
      return res.status(200).json({ message: "Valid User" });
    }
    return res.status(400).json({ message: "Invalid User" });

  } catch (_err) {
    return res.status(401)
  }
};

exports.pushTableData = async (req, res) => {
  const { P_ID, MeemmaseedTable, VussanutTable } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID });

    if (rootUser) {
      await User.updateOne(
        { P_ID },
        {
          $set: {
            MeemmaseedTable: MeemmaseedTable,
            VussanutTable: VussanutTable
          },
        }
      );

      return res
        .status(200)
        .json({ message: "Successfull" });
    }
  } catch (err) {
    return res.status(401).json({ error: "Couldn't update Meemmaseed Vussaut Table" });
  }
};

exports.trainingData = async (req, res) => {
  const { P_ID, TrainingData } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID });

    if (rootUser) {
      await User.updateOne(
        { P_ID },
        {
          $set: {
            TrainingData: TrainingData,
          },
        }
      );

      return res
        .status(200)
        .json({ message: "Successfull" });
    }
  } catch (err) {
    return res.status(401).json({ error: "Couldn't update Training Data" });
  }
};

exports.officialData = async (req, res) => {
  const { P_ID, OfficialData, Bonus } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID });

    if (rootUser) {
      await User.updateOne(
        { P_ID },
        {
          $set: {
            OfficialData: OfficialData,
            Bonus: Bonus
          },
        }
      );

      return res
        .status(200)
        .json({ message: "Successfull" });
    }
  } catch (err) {
    return res.status(401).json({ error: "Couldn't update Training Data" });
  }
};

exports.questionData = async (req, res) => {
  const { P_ID, QuestionData } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID });

    if (rootUser) {
      await User.updateOne(
        { P_ID },
        {
          $set: {
            questionnaire: QuestionData,
          },
        }
      );

      return res
        .status(200)
        .json({ message: "Successfull" });
    }
  } catch (err) {
    return res.status(401).json({ error: "Couldn't update Training Data" });
  }
};

exports.saveData = async (req, res) => {
  const { P_ID, learningRound, officialRound } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID });

    if (rootUser) {
      await User.updateOne(
        { P_ID },
        {
          $set: {
            learningRound: learningRound,
            officialRound: officialRound,
          },
        }
      );

      return res.status(201).json({ message: "Data saved successfully" });
    }
  }
  catch (err) {
    return res.status(401).json({ error: "Couldn't save data" });
  }
};

exports.saveDelegationDecision = async (req, res) => {
  const { P_ID, delegationDecision } = req.body;

  try {
    const rootUser = await User.findOne({ P_ID });

    if (rootUser) {
      await User.updateOne(
        { P_ID },
        {
          $set: {
            delegationDecision: delegationDecision
          },
        }
      );

      return res.status(201).json({ message: "delegation decision saved successfully" });
    }
  }
  catch (err) {
    return res.status(401).json({ error: "Couldn't save delegation decision" });
  }
};






























// exports.account = (req, res) => {
//   console.log(`Authenticated`);
//   res.send(req.rootUser);
// };

exports.login = async (req, res) => {
  const { email, password } = req.body;

  if (!email.trim() || !password.trim()) {
    return res.status(422).json({ error: "Please fill the entries properly" });
  }

  try {
    const userExist = await User.findOne({ email: email });

    if (!userExist) {
      return res.status(400).json({ error: "Invalid credentials" });
    }

    const isMatch = await bcrypt.compare(password, userExist.password);
    const token = await userExist.generateAuthToken();

    res.cookie("jwtoken", token, {
      expires: new Date(Date.now() + 180000000),
      httpOnly: true,
    });

    if (!isMatch) {
      return res.status(400).json({ error: "Invalid credentials" });
    } else {
      console.log("Login successful");
      res.json({
        message: "Login successful",
        _id: userExist._id,
        testAttempted: userExist.testAttempted,
        learningAttempted: userExist.learningAttempted,
      });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.learningAttempted = async (req, res) => {
  const { _id } = req.body;

  try {
    const rootUser = await User.findOne({ _id });

    if (rootUser) {
      await User.updateOne(
        { _id },
        {
          $set: {
            learningAttempted: true,
          },
        }
      );

      return res
        .status(200)
        .json({ message: "learningAttempted switched to true" });
    }
  } catch (err) {
    return res.status(401).json({ error: "Couldn't update learningAttempted" });
  }
};


exports.updateLearningRound = async (req, res) => {
  const { learningRound, _id } = req.body;

  try {
    const rootUser = await User.findOne({ _id });

    if (rootUser) {
      await User.updateOne(
        { _id },
        {
          $set: {
            learningRound: learningRound,
          },
        }
      );

      return res
        .status(200)
        .json({ message: "sent learning round data to database" });
    }
  } catch (err) {
    return res.status(401).json({ error: "Couldn't send learning round data to database" });
  }
}

exports.logout = (req, res) => {
  res.clearCookie("jwtoken", { path: "/" });
  console.log(`Signed out successfully.`);
  res.status(200).send("User signed out successfully.");
};

//signup using async await
exports.signup = async (req, res) => {
  const { name, email, password } = req.body;

  if (!name.trim() || !email.trim() || !password.trim()) {
    return res.status(422).json({ error: "Please fill the entries properly" });
  }

  try {
    const userExist = await User.findOne({ email: email });
    if (userExist) {
      return res.status(422).json({ error: "User already exists" });
    }

    const user = new User({
      name,
      email,
      password,
    });

    //hashing "password" and "confirm_password" before saving them to the database
    await user.save();

    res.status(201).json({ message: "User signup successful" });
  } catch (err) {
    console.log(err);
  }
};

exports.getResult = async (req, res) => {
  const { _id } = req.body;

  try {
    const rootUser = await User.findOne({ _id });

    if (rootUser) {
      console.log("result api call successful");
      res.json({
        learningRound: rootUser.learningRound,
        officialRound: rootUser.officialRound,
        feedbackRound: rootUser.feedbackRound,
        questionnaire: rootUser.questionnaire
      });
    }
  } catch (err) {
    console.log(err);
  }
};

const mongoose = require('mongoose');

const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
let random = '';
for (let i = 0; i < 24; i++) {
    random +=
        characters.charAt(Math.floor(Math.random() * 36));
}


const userSchema = new mongoose.Schema({
    P_ID: {
        type: String,
        unique: true,
    },
    Date: {
        type: Date,
        default: Date.now()
    },
    kickedOut: {
        type: Boolean,
        default: false,
    },
    testAttempted: {
        type: Boolean,
        default: false,
    },
    MeemmaseedTable: [],
    VussanutTable: [],
    TrainingData: [],
    questionnaire: {},
    OfficialData: [],
    Bonus: {},
    userData: {},
    treatment: {
        type: Number
    },
})

//collection creation
const User = mongoose.model("Treatment5", userSchema);

module.exports = User;